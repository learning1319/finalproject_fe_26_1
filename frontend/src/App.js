
import './App.css';
import { useEffect } from "react";

function App() {
  useEffect(() => {
    fetch('http://localhost:4000/api/products')
      .then(res => res.json())
      .then(result => console.log(result))
  }, []);
  return (
    <div className="App">
      <h1>Hello</h1>
    </div>
  );
}

export default App;
